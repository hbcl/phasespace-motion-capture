You can update the firmware on your Stylus Units (microdrivers) using a program that is on the server called fwload, which you should run as root and make sure that you are not streaming when you run it. The command to update microdriver firmware is:

    fwload --microdriver

Once this is running the program will be waiting for you to connect a stylus (microdriver) to a USB port on the server. Before you connect it to the server, you must make sure that the microdriver is completely off (to turn the microdriver completely off, you should hold the white button on the microdriver until the indicator light on the microdriver stops flashing). While plugging into the USB you will have to hold the red button on the microdriver down. You should notice some output on the screen, and then it will be ready for the next microdriver to be plugged in.

You should power cycle the microdriver once the firmware has been updated.