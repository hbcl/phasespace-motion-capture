# Welcome to PhaseSpace 
## Critical potential error: ALWAYS turn off server box BEFORE tugging on the cables connecting cameras. Unplugging the ethernet cables from cameras while they are powered on can do $5000 damage per camera.  
## For this reason, please turn off the server when not in use!

This readme documents how to setup phasespace. It includes a list of common troubleshooting problems and error messages.

# Log
* 2022-08-11: added link for installing masterclient
* 2020-03-11: added note about network connection setp
* 2019-08-08: Updated by SP to include new errors, info on camera placement, and edits to most sections
* 2019-07-03: updated by SP to include new errors and link to driver spreadsheet
* 2019-06-28: updated by SP
* 2019-06-20: updated to merge JDW and SM's notes. 
* 2019-06-19: edited SM's notes for clarity.
* 2019-06-18: moved SM's .docx to markup

# Troubleshooting and Error messages 

## Other errors 

* Cannot get LEDs to turn on
Check to see if you are exceeding the maximum number of pre-encoded profiles. i.e. Have no more than 4 pre-encoded profiles.

* Synchronization program crashes:
The synchronization program will crash without warning sometimes. Cause unknown. 

* At calibration, repeated turning off of ireds, failure to calibrate:
Even if the driver should be charged and good, try switching it out.
Make sure that the brightness is set to a surprisingly-low value of 13 or so.

* [paraphrase] Cannot initialize without cameras. Even though there are cameras plugged in.
Some server ports appear to be broken, and an RF camera (with the wireless antenna) needs to be seen. 
We suspect Port 3 is broken and there may be others. 

* Within Master Client program: "cannot connect host as a slave."
This seems to occur somewhat randomly; unclear what fixes this. Power cycling possibly works. This error message can also 
be thrown if the synchronization program is started before Master Client, but it's not the only cause. This error message will also be thrown if the hub is streaming to cameras.
To solve this, log on to the Phasespace server, go to "Hub Status", and de-select the magnifying glass. 

* Calibration Errors:
    * "failure, pairings unmatched". Image: https://imgur.com/a/afDGo3c 

    * "failure, error bounds exceeded". Image: https://imgur.com/a/m0m69Ag

    * "failure, cameras unmerged". Image: https://imgur.com/cONP9bZ
    * "failure, no frames captured." [same error location as above]
    
Many calibration errors are caused by poor camera placement or simply not capturing enough frames. Try capturing more frames and/or optimizing your camera setup to prevent these errors.

2024-10-15 note: we received repeated 'pairings failed', 'pairings unmatched', and 'camera x no data.' They were resolved by a full restart of both server and control computer. Not sure how common this is likely to be.  

* When opening the IP address using internet explorer will give a 'site error 0' and not open.
    This problem was fixed when we opened the IP address with google chrome. 

* 404 error connecting to server
The ethernet adapter needs to have a static IP address that works with the IP address hard-coded on the Phasespace server. what works is 192.168.1.10 and subnet mask 255.255.255.0 set in  Windows Network Connections ->
Ethernet (Local Area Connection) <right click> and -> Properties Select Internet Protocol Version 4 (TCP/IPv4) -> Properties.



###Driver Errors
* Some microdivers are not working. Please use the following spreasheet to keep track of which drivers are working. Record driver status and errors as is relevant. 
Link: https://docs.google.com/spreadsheets/d/1RoPK8Us-5WknCEAcxrZzVfDCnCY0bt_Ic4H2azUCS-U/edit?usp=sharing


# Phase Space Instructions 

## Setup! A: Camera setup; B: Server and driver setup; C: Phasespace Master Client Connection and calibration

### A: Camera setup
A good camera set up will make your life significantly better. If you are using the phasespace system with the Bertec treadmill, the cameras are likely already set up for you. 
If you are using the system to capture a different area than the treadmill, you will benefit from putting some thought into your camera placement and set up. 

The phasespace server can (in theory) power 48+ cameras. Cameras are powered by ethernet cables and are daisy chained together. The maximum number of cameras that you can daisy chain is (probably?) 7.

To accurately capture the position of a point, it works best if at least 3 cameras can see that point. 
Use this guideline when aligning the cameras; make sure that all points in your capture space can be seen by at least 3 cameras.

A good place to start is often by arranging your cameras in a circle around the capture area. From here, you can work towards more clever camera placements. 
Remember that not every camera has to see the entire capture area, but each camera should do a good job of covering a specific area. Thinking through your camera placement will save you time in the long run.

It's always a good idea to test your camera setup. Once your cameras are placed record some data and see what it looks like.
If your data is full of occlusions, go back and adjust your cameras. **This will make post-processing easier and is totally worth your time**


### B: Server and driver setup
**Only once you have your cameras set up and plugged in**, turn on the 2018 server with the power button located on the front. The 2018 server is currently the only working one.

Camera chains must be plugged into server prior to powering on. The server must also be plugged into the desktop.

It should look like this: https://imgur.com/a/J6cp3H9 and https://imgur.com/a/cZ0sa9f

Using google chrome, go to IP address **192.168.1.230** (error using explorer). Note: this means the IPv4 settings for the recording computer needs to have a "192.168.1" prefix, and the subnet mask needs to be 255.255.255.0. (set in IPv4 settings in windows [for the correct card!]).

You will be prompted to log on.

* Username: admin
* Password: phasespace

Copy PhasespaceFrameLogger_960Hz and the .lib file (both in D:\DataCollectionAnalysisTemplates\PhaseSpaceFrameLoggerAndTreadmill) to the folder where the data is written to. 

Next, run/open: 

* Master Client (for motion capture only)
* Synchronization program (PhasespaceFrameLogger_960Hz) and Matlab2012b->TreadmillForceData (for force plates)
* Treadmill control pannel (for treadmill control)

In the web browser, go to "Hub Status" and click the magnifying glass. Cameras should appear in the window. Double check to see if the correct number of cameras appears.
If you are missing cameras, turn the server off and then check the camera connections. 

Three parts:
1. LED Devices
2. Session Profiles
3. Web Clients (not used as of 2019-08-08)

#### LED Devices:
This lists the microdrivers that are currently on and visible to the system (look at H/W ID for identification).

    If LED connected: driver ID should be visible. If no driver ID's are visible, double check to see if you have an RF camera (i.e. a camera with antenna) in your set-up. 
    If you do not have an RF camera, the server will not be able to communicate with the microdrivers.

Now you _must_ select (enable) one driver for calibration, and for your Driver setup to be pre-encoded. It is advisable to select a driver with full battery life (look at the Battery column).
Click (highlight) the driver and click the lower settings button (gear, top right) and select enable for calibration. Now (calib) appears before the driver name.

#### Session Profiles: Driver/LED information, settings and setup
Here you _must_ have a Session profile for your experiment. These profiles therefore need to be done once per experiment really, 
and then you just choose that one each time you run the experiment.
 
 To Create a _Session profile_, you will need to add several drivers to a profile, configure them (double click to open) so that the right number of LEDS are on per driver (define the group each LED is associated to).
 
 Select driver -> left menu -> shows groups of LEDs by colors. Each LED is defined by it's ID (under strings column) and LED group. 

 Now add: your session profile to the _Pre-encode_ (on 'LED Devices' page) list. _DO NOT_ have more than 4 pre-encoded profiles.
 
 [note: Battery indicator is not working well (full battery lasts a couple of hours, holding white button on wand turns it off, pressing white button turns it on)]
 
 
#### Web Clients: (not useful at the moment)


### C: Master Client Connection and Calibration:
Open PhaseSpace Master Client, under owl configuration (top left):

1.    insert the server IP 192.168.1.230 under address
2.    Select session profile
3.    choose the right frequency
4.    choose the right interpolation.
5.    choose the right LED power (13)
6.    connect
7.    calibrate
8.    setup synchronized recording if applicable.

Profile: select the one that was created and saved earlier

Keep alive: turned off

Frequency: 960/480/240/120Hz depending on your aim and nr of markers 

Interpolate: 2 	(we do #groups+1. At least that, if set to a lower number -> raw output, if set to 0 -> projection instead of interpolation)

Smooth: (set to the same as interpolate)

LED POWER: _VERY IMPORTANT!_ 15 SEEMS FINE. Apparently too high has been bad before.  

BUTTON Connect 	(license warning that can be ignored)

**Important** Under settings -> Recording, make sure that "Record C3D" and "enable triggered recording" are checked, otherwise you're going to have a bad time. 

#### Calibration
Prior to calibration go to the browser, under LED devices click the start stream button ('eye' symbol, left) to stop streaming data, the drivers should now all be greyed out. Then disconnect and reconnect the master client.  

Tools -> Calibration	(perhaps control LED power/brightness here)

			Yellow lines: too bright
            
			Red lines: too dark
			
Use wand and attach calibration driver, needs to show white dots in camera field of views (FOV), representing LEDs on wand.
Set LED power according to yellow and red lines (usually 13 power is fine)

1. Calibration Object: Wand 8 LED
2. Capture: at least inner 3x3 squares should be green (in general 80% of FOVs, capture stops when “stop” button is being hit.
3. Calibrate: exhaustive.

Wave the wand around the capture area until you get enough green in the boxes (more is better). Do a dance and have some fun with this if you want. 

Calibration is also a good time to check your camera setup; if you have to go way out of your capture area to calibrate, you might need to rethink your camera placement.

When finishing calibration, check the error number. If it's less than 1.0, you're probably having a good day. If it's really high, something might be off. 
Either way, make sure to walk around your capture area with some markers on and make sure that the data that you are collecting is good.


In the calibration panel -> Align: 	Put wand with driver side up into screws at three locations to define the coordinate system. 
The order (important!) will define your coordinate system, and is as follows: origin, positive x-axis, positive z-axis 
(y-axis is defined by right hand rule).    
	(if on your own: auto snapshot takes snapshot when wand is not being moved)
    
	Done --> Save!

#### Collect:

Take Name: 	filename 	(1st = filename, 2nd = filename _01, etc..)

File -> Working Directory (select directory)

#### Integrate Treadmill:
(if not needed, just press record button to start recording)

Master client Menu -> Settings

Recording: enable triggered recording, trigger behaviour: press start/ release stop

Record CJD enables

Record Start delay enabled (at least 1ms)

“Apply” -> Close

On the treadmill computer we have a directory of template data collection files. they are at D:\DataCollectionAnalysisTemplates.

The one we want for Motion capture, Forces, and the logger (which connects the two in post processing) is 
D:\DataCollectionAnalysisTemplates\PhaseSpaceFrameLoggerAndTreadmill. Copy that template to your D:\Data\$YOURNAME\ directory. 

(if you copy this folder and set the matlab and Make sure the files PhaseSpaceTimeLogger_960Hz.exe and libowlsock.dll are in the current phasespace directory, ie the directory where phasespace is going to write data to.

Run the file PhaseSpaceTimeLogger_960Hz.exe, this should give you a terminal in which information on the framelogger's status is displayed.

Now take data using the treadmill controls, make check that a .txt file is written by the framelogger in the phasespace directory.

For integrating/synchronizing phasespace and treadmill data in matlab, see: hbcl-toolbox/PhaseSpaceToolbox/phaseSpaceAndBertec_sync_code

## Treadmill controls
Open Matlab 2012b 32 bit 

Open file: treadmillforcedata.m file can be found at E:\DataCollectionAnalysisTemplates\treadmilltemplate copy if used

Open Treadmill Control Panel (fine to search 'treadmill control' in Windows start menu; at the moment there are actually two progs so if you cannot control the treadmill speed/incline, you're using the wrong one. 

## A note about the frequencies used in this system! 
Camera sampling frequency: 	960Hz	(10 cameras)

Marker effective frequency: 	960Hz (max) 480Hz (for 2 groups), 240Hz 	(for 4 groups), etc.

### OLD Phasespace tutorial documentation ###
https://paper.dropbox.com/doc/Experiment-Setup-Treadmill-Computer-and-Phasespace-KUOLab--AfTTBIcOMmFxCU0TgH2cj8wkAQ-dzLetJCzS28f8DybKED8I

### What else is this repository for? ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Who do I talk to? ###

* Jeremy, Xiao-Yu, Hansol, Koen, Sam, and soon Austin have all now used the new Phasespace setup.

### software install
masterclient: customers.phasespace.com/anonymous/Software/5.2
server software: customers.phasespace.com/anonymous/Packages/5.2
u: anonymous p: guest

#### to install server:
put it on the phasespace server, 
ssh in root su phasespace pw

dpkg -r phasespace-core (on the main server we use)
dpkg -P phasespace-core (purge)

both cases, to reinstall, dpkg -I phasespace-core_5.2.150+182+190-2_amd64.deb


## Image Hosting ##

All the images linked to in this doc are hosted on imgur. To uplaod more images, login in to the account with 

Username: KuoLab
Password: ScienceRules!1